if NOT EXISTS (SELECT 1 FROM MNU_INDICATOR_VALUES WHERE SYS_INDICATOR_ID=39)
  INSERT INTO MNU_INDICATOR_VALUES
  VALUES 
  (39, 
  'FUNCTIONAL_GROUP_IND', 
  21, 
  'Marketer',
  21)

exec dbo.LSP_UTIL_LOG_SQL_SCRIPT
	@ScriptFileName = '2020 05 13 Add Marketer group.sql',
	@Author = 'Sachin Borgaonkar',
	@Description = 'Added new entry for FUNCTIONAL_GROUP_IND of Marketer .'
  