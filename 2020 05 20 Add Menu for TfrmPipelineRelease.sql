--Add to new home page menu system
DECLARE 
	@FormObjectHandle varchar(50) ,
	@FormName varchar(50),
	@FormHint varchar(50),
	@FormShortName varchar(100),
	@FullAccessGroupID Int,
	@Resource_Num varchar(50),
	@FormTypeID int,
	@FunctionalGroup int,
	@ScriptFileName varchar(100)

set @FormTypeID = 100 --TForm
set @FormObjectHandle = 'TfrmPipelineRelease'
set @FormName = 'Pipeline Release Contracts'
set @FormHint = 'Pipeline Release Contracts'
set @FormShortName = 'Pipe Release'
set @Resource_Num = 'Resource_265'
set @FullAccessGroupID = 1000 --Full Access
set @FunctionalGroup = 20  --SELECT * FROM MNU_INDICATOR_VALUES WHERE SYS_IND_FIELD_NAME = 'FUNCTIONAL_GROUP_IND'
set @ScriptFileName = '2020 05 20 Add Menu for TfrmPipelineRelease'

IF NOT EXISTS (SELECT 1 FROM MNU_FORM WHERE FORM_HANDLE = @FormObjectHandle)
INSERT INTO [dbo].[MNU_FORM]
           ([FORM_NAME]
           ,[SHORT_NAME]
           ,[FORM_HINT]
           ,[FORM_HANDLE]
           ,[FUNCTIONAL_GROUP_IND]
           ,[IMAGE_ID]
           ,[RESOURCE_ID]
           ,[LAST_UPDATE]
           ,[LAST_UPDATE_BY])
     VALUES
           (@FormName--<FORM_NAME, varchar(250),>
           ,@FormShortName --<SHORT_NAME, varchar(100),>
           ,@FormHint--<FORM_HINT, varchar(500),>
           ,@FormObjectHandle--<FORM_HANDLE, varchar(100),>
           ,@FunctionalGroup--<FUNCTIONAL_GROUP_IND, int,>
           ,100--<IMAGE_ID, int,>
           ,@Resource_Num--<RESOURCE_ID, varchar(50),>
           ,getdate()--<LAST_UPDATE, datetime,>
           ,null-- <LAST_UPDATE_BY, int,>)
					 )

IF NOT EXISTS (SELECT 1 FROM SEC_SECURITY_OBJECTS O WHERE O.SEC_OBJECT_NAME = @FormObjectHandle)
INSERT INTO SEC_SECURITY_OBJECTS VALUES 
	(@FormTypeID, @FormObjectHandle,NULL,@FormName)

IF NOT EXISTS (
	SELECT 1 
	FROM SEC_USER_GROUP_PERMISSIONS P
	JOIN SEC_SECURITY_OBJECTS O ON O.SEC_OBJECT_ID = P.SEC_OBJECT_ID
	WHERE O.SEC_OBJECT_NAME = @FormObjectHandle)
INSERT INTO SEC_USER_GROUP_PERMISSIONS  
	SELECT 
		O.SEC_OBJECT_ID,
		@FullAccessGroupID, 
		'Y',
		'Y'
	FROM SEC_SECURITY_OBJECTS O 
	WHERE O.SEC_OBJECT_NAME = @FormObjectHandle

declare 
	@filename varchar(100), 
	@Author varchar(35), 
	@Description varchar(100)


set @Description = 'Add to Menu for ' + @FormObjectHandle
set @Author = 'Sachin Borgaonkar'	

exec dbo.LSP_UTIL_LOG_SQL_SCRIPT
	@ScriptFileName = '2020 05 20 Menu TfrmPipelineRelease',
	@Author = @Author,
	@Description = @Description