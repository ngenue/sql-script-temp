if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].LSP_SEL_COUNTERPARTY_CONTRACTS') 
		and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].LSP_SEL_COUNTERPARTY_CONTRACTS
GO

-- Usage :
-- EXEC LSP_SEL_COUNTERPARTY_CONTRACTS                         List all contracts
-- EXEC LSP_SEL_COUNTERPARTY_CONTRACTS NULL,NULL,NULL,NULL     List all contracts
-- EXEC LSP_SEL_COUNTERPARTY_CONTRACTS 1003, 2                 List of Contracts's where PIPELINE_ID = 1003
-- EXEC LSP_SEL_COUNTERPARTY_CONTRACTS NULL, 2                 List all Pipeline Contracts's
-- EXEC LSP_SEL_COUNTERPARTY_CONTRACTS NULL, 2,'05/05/2020'    List of Pipeline Contracts's where 05/05/2020 between Start & End Dt
-- EXEC LSP_SEL_COUNTERPARTY_CONTRACTS NULL, NULL,'05/05/2020' List of all Contracts's where 05/05/2020 between Start & End Dt
-- EXEC LSP_SEL_COUNTERPARTY_CONTRACTS NULL, 1,NULL,'Trad'     List of all Supplier Contracts's with Supplier Name like 'Trad'
-- EXEC LSP_SEL_COUNTERPARTY_CONTRACTS NULL, 2,'05/05/2016','Gas Energy'  List of all Pipeline Contracts's with Pipeline Name like 'Gas Energy' and 05/05/2016 between Start & End Dt
CREATE PROCEDURE [dbo].[LSP_SEL_COUNTERPARTY_CONTRACTS]
 @CP_ENTITY_ID INT = 0,  
 @COUNTERPARTY_TYPE_ID INT = 0,
 @INCLUDE_DATE DATETIME = '01/01/1905',  
 @SearchStr VARCHAR(500) = ''  
AS  
BEGIN  
 IF @COUNTERPARTY_TYPE_ID IS NULL  
   SET @COUNTERPARTY_TYPE_ID = 0 
 IF @CP_ENTITY_ID IS NULL  
   SET @CP_ENTITY_ID = 0 
 IF @INCLUDE_DATE IS NULL  
   SET @INCLUDE_DATE = '01/01/1905'  

 SELECT ISNULL(S.NAME, ISNULL(P.NAME, ISNULL(OC.LAST_NAME, ''))) AS CONTRACT_ENTITY_NAME, 
 CP_T.CP_TYPE_NAME,  
 C.*   
 FROM COUNTERPARTY_CONTRACT C  
 JOIN COUNTERPARTY_TYPE CP_T ON CP_T.COUNTERPARTY_TYPE_ID = C.COUNTERPARTY_TYPE_ID  
 LEFT OUTER JOIN SUPPLIER S ON S.SUPPLIER_ID = C.SUPPLIER_ID  
 LEFT OUTER JOIN PIPELINE P ON P.PIPELINE_ID = C.PIPELINE_ID  
 LEFT OUTER JOIN OUTSIDE_CONSULTANT OC ON OC.OUTSIDE_CONSULTANT_ID = C.OUTSIDE_CONSULTANT_ID  
 WHERE   
    (@INCLUDE_DATE = '01/01/1905'   -- For NULL date
	      OR (@INCLUDE_DATE > '01/01/1905' AND (@INCLUDE_DATE BETWEEN C.[START_DATE] AND C.END_DATE)))
		  
    AND (@COUNTERPARTY_TYPE_ID = 0 
	        AND ((@CP_ENTITY_ID = 0 AND C.COUNTERPARTY_TYPE_ID >= 0) OR   --All Counterparty Types
	             (@CP_ENTITY_ID > 0 AND C.COUNTERPARTY_CONTRACT_ID = @CP_ENTITY_ID))
			) 
    OR (@COUNTERPARTY_TYPE_ID > 0 AND C.COUNTERPARTY_TYPE_ID = @COUNTERPARTY_TYPE_ID
	    AND (@CP_ENTITY_ID = 0
		     OR (@CP_ENTITY_ID > 0 AND (
			                           (@COUNTERPARTY_TYPE_ID = 1 AND C.SUPPLIER_ID = @CP_ENTITY_ID)
	                                OR (@COUNTERPARTY_TYPE_ID = 2 AND C.PIPELINE_ID = @CP_ENTITY_ID)
			                        OR (@COUNTERPARTY_TYPE_ID = 7 AND C.OUTSIDE_CONSULTANT_ID = @CP_ENTITY_ID)
			                        OR (@COUNTERPARTY_TYPE_ID = 5 AND C.COUNTERPARTY_CONTRACT_ID = @CP_ENTITY_ID)
									)
				)
	        )
		)
	  
	AND (@SearchStr = '' 
	        OR (@SearchStr <> '' AND @CP_ENTITY_ID > 0)   
		    OR (@SearchStr <> '' AND @CP_ENTITY_ID = 0 AND (
			                          (@COUNTERPARTY_TYPE_ID = 1 AND S.NAME LIKE '%'+ @SearchStr+ '%')
	                               OR (@COUNTERPARTY_TYPE_ID = 2 AND P.NAME LIKE '%'+ @SearchStr+ '%')
			                       OR (@COUNTERPARTY_TYPE_ID = 7 AND OC.LAST_NAME LIKE '%'+ @SearchStr+ '%')
								   OR (@COUNTERPARTY_TYPE_ID in (0,5) AND C.CP_CONTRACT_NAME LIKE '%'+ @SearchStr+ '%')
								   )
		     )
		)

    ORDER BY ISNULL(S.NAME, ISNULL(P.NAME, ISNULL(OC.LAST_NAME, '')))
END
GO

exec dbo.LSP_UTIL_LOG_SQL_SCRIPT
	@ScriptFileName = '2020 05 08 LSP_SEL_COUNTERPARTY_CONTRACTS.sql',
	@Author = 'Kishore Sharma',
	@Description = 'Modified LSP_SEL_COUNTERPARTY_CONTRACTS.'

