if exists (select * from dbo.sysobjects 
	where id = object_id(N'[dbo].SUPPLYDEALINSTRIG') 
		and OBJECTPROPERTY(id, N'IsTrigger') = 1)
  drop TRIGGER [dbo].SUPPLYDEALINSTRIG
GO

CREATE TRIGGER [dbo].[SUPPLYDEALINSTRIG] ON [dbo].[SUPPLYDEAL]
FOR INSERT
AS
    DECLARE @RowCount INT
    DECLARE @NEW_SUPPLIER_ID INT
    DECLARE @NEW_SUPPLY_DEAL_ID INT 
    DECLARE @NEW_STARTDATE DATEFIELD
    DECLARE @NEW_ENDDATE DATEFIELD
		
    DECLARE @CURRENTDATE DATEFIELD
    DECLARE
			@ACCOUNTINGGROUPID INT, 
			@BUSINESS_UNIT_ID INT, 
			@SUPPLYREGIONID INT,
			@PORTFOLIO_ID INT,
			@PERIOD_TYPE INT,
			@STRATEGY_ID INT,
			@PIPELINE_ID INT,
			@PIPELINE_LOCATION_ID INT,
			@INSTRUMENT_ID INT,
			@TRXTYPE VARCHAR(25),
			@DAILYVOLUME NUMERIC(15,2)
	DECLARE @NEW_COUNTERPARTYTYPE SHORTNAME
    DECLARE NEW CURSOR LOCAL
        FOR SELECT SUPPLIER_ID, SUPPLY_DEAL_ID, STARTDATE, ENDDATE, ACCOUNTINGGROUPID, 
					BUSINESS_UNIT_ID, SUPPLYREGIONID, PORTFOLIO_ID, STRATEGY_ID, 
					PIPELINE_ID, PIPELINE_LOCATION_ID, TRXTYPE, DAILYVOLUME, INSTRUMENT_ID, PERIOD_TYPE_REF
       FROM INSERTED
BEGIN
    OPEN NEW
    FETCH NEW INTO @NEW_SUPPLIER_ID, @NEW_SUPPLY_DEAL_ID, @NEW_STARTDATE, @NEW_ENDDATE, 
					@ACCOUNTINGGROUPID, @BUSINESS_UNIT_ID, @SUPPLYREGIONID, @PORTFOLIO_ID, @STRATEGY_ID,
					@PIPELINE_ID, @PIPELINE_LOCATION_ID, @TRXTYPE, @DAILYVOLUME, @INSTRUMENT_ID,@PERIOD_TYPE
    WHILE (@@FETCH_STATUS = 0)
    BEGIN
       DECLARE @DATE_IS_CLOSED CHAR(1)
       EXEC SP_TEST_CLOSEDATE @NEW_ENDDATE,
          @out_PRIORTOCLOSE = @DATE_IS_CLOSED OUTPUT
        
       IF @DATE_IS_CLOSED = 'Y'
       BEGIN
         RAISERROR('You may not modify records prior to Accounting Close Date',16,1)
         ROLLBACK RETURN 
       END
      /*
       * Child Insert@ RESTRICT
       */
			SELECT @RowCount = COUNT(*)
			FROM Supplier pr
			WHERE pr.Supplier_ID = @NEW_Supplier_ID

			IF (@RowCount < 1)
			BEGIN
				RAISERROR('Insert Supply Deal Failed: Supplier does not exist.', 16, 1) /* 'Exception E_INS_Supplier */
				ROLLBACK RETURN
			END
			
      
      if dbo.[udf_CheckSysRequired_AccountingGroup] () = 'Y' 
      BEGIN
				SELECT @Rowcount = count(*)
				from ACCOUNTINGGROUP
				WHERE ACCOUNTINGGROUPID = @ACCOUNTINGGROUPID
				IF (@RowCount < 1)
				BEGIN
					RAISERROR('System requires an accounting group for each supply deal.', 16, 1) /* 'Exception E_INS_Supplier */
					ROLLBACK RETURN
				END
      END
      if dbo.[udf_CheckSysRequired_BusinessUnit] () = 'Y' 
      BEGIN
				SELECT @Rowcount = count(*)
				from BUSINESS_UNIT
				WHERE BUSINESS_UNIT_ID = @BUSINESS_UNIT_ID
				IF (@RowCount < 1)
				BEGIN
					RAISERROR('System requires an business unit for each supply deal.', 16, 1) /* 'Exception E_INS_Supplier */
					ROLLBACK RETURN
				END
      END
      if dbo.[udf_CheckSysRequired_SupplyRegion] () = 'Y' 
      BEGIN
				SELECT @Rowcount = count(*)
				from SUPPLYREGION
				WHERE SUPPLYREGIONID = @SUPPLYREGIONID
				IF (@RowCount < 1)
				BEGIN
					RAISERROR('System requires a supply reqion for each supply deal.', 16, 1) /* 'Exception E_INS_Supplier */
					ROLLBACK RETURN
				END
      END
      
      if dbo.[udf_CheckSysRequired_Portfolio] () = 'Y' 
			BEGIN
				SELECT @Rowcount = count(*)
				from PORTFOLIO
				WHERE PORTFOLIO_ID = @PORTFOLIO_ID
				IF (@RowCount < 1)
				BEGIN
					RAISERROR('System requires a portfolio for each Supply Deeal.', 16, 1) 
					ROLLBACK RETURN
				END
			END
			
			if dbo.[udf_CheckSysRequired_Strategy] () = 'Y' 
			BEGIN
				SELECT @Rowcount = count(*)
				from STRATEGY
				WHERE STRATEGY_ID = @STRATEGY_ID
				IF (@RowCount < 1)
				BEGIN
					RAISERROR('System requires a strategy for each Supply Deeal.', 16, 1) 
					ROLLBACK RETURN
				END
			END
      
			--10-08-2018 new conditional enforcement of curve
			if dbo.[udf_CheckSysRequired_MarkToMarket] () = 'Y' 
			BEGIN
				SELECT @Rowcount = count(*)
				from PIPELINE  
				WHERE PIPELINE_ID = @PIPELINE_ID
				IF (@RowCount < 1)
				BEGIN
					RAISERROR('System market curve setting requires Pipeline for supply deals.', 16, 1) 
					ROLLBACK RETURN
				END

				SELECT @Rowcount = count(*)
				from PIPELINE_LOCATION    
				WHERE PIPELINE_LOCATION_ID = @PIPELINE_LOCATION_ID
				IF (@RowCount < 1)
				BEGIN
					RAISERROR('System market curve setting requires pipeline location.', 16, 1) 
					ROLLBACK RETURN
				END	
			END

			IF @INSTRUMENT_ID = 1 --Physical
			begin
				EXEC LSP_SUPPLY_INSERT_DAILY_DEAL_VOLS
				            @NEW_SUPPLY_DEAL_ID, 'N', @PERIOD_TYPE
				
/*				INSERT INTO SUPPLY_DEAL_DAILY_VOLUME (
					 SUPPLY_DEAL_ID 
					,DEALDATE
					,VOLUME
					,REC_OR_DEL
					,PIPELINE_LOCATION_ID
					,SUPPLYTIERID 
					,NOTE
					,DT_UPDATED
					,UPDATEDBY
					,NOMINATION_BAV_TYPE_ID
					,IS_BAV
					)
				select 
					@NEW_SUPPLY_DEAL_ID
					,D.THE_DATE
					,@DAILYVOLUME
					,CASE @TRXTYPE WHEN 'Buy' THEN 'R' ELSE 'D' END
					,@PIPELINE_LOCATION_ID
					,NULL
					,''
					,GETDATE()
					,SYSTEM_USER
					,1 -- Contractual Qty
					,'Y'
				FROM UTIL_DATES D
				WHERE D.THE_DATE BETWEEN @NEW_STARTDATE AND @NEW_ENDDATE
*/			END

      FETCH NEW INTO @NEW_SUPPLIER_ID, @NEW_SUPPLY_DEAL_ID, @NEW_STARTDATE, @NEW_ENDDATE, 
					@ACCOUNTINGGROUPID, @BUSINESS_UNIT_ID, @SUPPLYREGIONID, @PORTFOLIO_ID, @STRATEGY_ID,
					@PIPELINE_ID, @PIPELINE_LOCATION_ID, @TRXTYPE, @DAILYVOLUME, @INSTRUMENT_ID,@PERIOD_TYPE
    END
    CLOSE NEW
    DEALLOCATE NEW
END
GO

ALTER TABLE [dbo].[SUPPLYDEAL] ENABLE TRIGGER [SUPPLYDEALINSTRIG]
GO

exec dbo.LSP_UTIL_LOG_SQL_SCRIPT
	@ScriptFileName = '2020 05 01 SUPPLYDEALINSTRIG.sql',
	@Author = 'Kishore Sharma',
	@Description = 'Support for Monthly in SUPPLY_DEAL_DAILY_VOLUME.'


