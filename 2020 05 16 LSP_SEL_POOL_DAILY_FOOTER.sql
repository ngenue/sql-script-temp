if exists (select * from dbo.sysobjects 
	where id = object_id(N'[dbo].LSP_SEL_POOL_DAILY_FOOTER') 
		and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].LSP_SEL_POOL_DAILY_FOOTER
GO

CREATE PROCEDURE dbo.LSP_SEL_POOL_DAILY_FOOTER
@PoolLookup int = 0,  
  @LDCLookup int = 0,  
  @MultiPool BIT = 0,  
  @CreateMultiPoolIDList varchar(max),  
  @meStart DateTime,  
  @meEnd DateTime, 
  @MARKETER_ID INT 
AS  
BEGIN  
 create table #ListTable (  
  val varchar(max)  
  )  
  if len(ISNULL(@CreateMultiPoolIDList,'')) > 0  
  begin  
  INSERT into #ListTable   
    select   
     T2.item.value('(./text())[1]','int') as val   
  from  
     (select convert(xml,'<items><t>'+replace(@CreateMultiPoolIDList,',','</t><t>')+'</t></items>') as xmldoc)  
  as xmltable  
     CROSS APPLY xmltable.xmldoc.nodes('/items/t') as T2(item)  
  end  
    SELECT   
    SUM(PURCHASEDBALANCEMM) AS PURCHASEDBALANCEMM,   
       SUM(BALANCEUSED) AS BALANCEUSED,   
       SUM(USAGEPERLDC) AS USAGEPERLDC,   
    SUM(AI_FORECAST) AS AI_FORECAST,  
    CONVERT(NUMERIC(15,2),0) AS LATEST_AI_FORECAST,  
       SUM(GATEDELIVERY) AS GATEDELIVERY,   
       SUM(CASHOUTS) AS CASHOUTS,   
       SUM(PENALTYVOLUME) AS PENALTYVOLUME,   
       SUM(ADJUSTMENT) AS ADJUSTMENT,   
       SUM(POOLNOMINATION) AS POOLNOMINATION,   
       SUM(TODAYSIMBALANCE) AS IMBALANCE,   
       SUM(TRADEDVOLUME) AS TRADES,   
       SUM(REQUIREDLDCDELIVERY) AS REQUIREDLDCDELIVERY,   
       SUM(POOLMDQ) AS POOLMDQ,   
       SUM(POOLMDN) AS POOLMDN,  
    HoldNet = ISNULL(SUM(GATEDELIVERY - (GATEDELIVERY * FUELLOSS / 100)), 0),  
    NetDelivery = SUM(case when FUELLOSS IS NULL then GATEDELIVERY ELSE 0 END)  
       FROM DAILYPOOLSTORAGE D
	   JOIN LDCPOOLINGACCOUNT P ON P.LDC_POOLING_ID = D.LDC_POOLING_ID
	   LEFT OUTER JOIN MARKETER M ON M.MARKETER_ID = P.MARKETER_ID   
       WHERE 1=1  
    AND(@LDCLookup = 0 or D.LDCID= @LDCLookup)  
      AND((@MultiPool = 0 AND D.LDC_POOLING_ID = @PoolLookup) or  
    (@MultiPool = 1 AND len(@CreateMultiPoolIDList) > 0 AND D.LDC_POOLING_ID in (select val from #ListTable))) 
	AND (@MARKETER_ID = 0 OR M.MARKETER_ID = @MARKETER_ID)  
       AND POOLDATE >= '' + @meStart + ''   
       AND POOLDATE <= '' + @meEnd + ''  
END
GO

grant exec on LSP_SEL_POOL_DAILY_FOOTER to EnTrackUser
GO


exec dbo.LSP_UTIL_LOG_SQL_SCRIPT
	@ScriptFileName = '2020 05 16 LSP_SEL_POOL_DAILY_FOOTER.sql',
	@Author = 'Mark Lloret',
	@Description = 'Description.'
