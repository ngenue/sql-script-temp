if exists (select * from dbo.sysobjects 
	where id = object_id(N'[dbo].LSP_SEL_DAILY_METER_FACILITY') 
		and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].LSP_SEL_DAILY_METER_FACILITY
GO

CREATE PROCEDURE dbo.LSP_SEL_DAILY_METER_FACILITY
 @FACILITY_ID INT = 0,  
 @LDCID INT = 0,  
 @POOLID INT = 0,  
 @SDATE DATETIME,  
 @EDATE DATETIME,  
 @COMP_DATE DATETIME,  
 @SORT_ORDER INT,
 @MARKETER_ID INT
AS  
BEGIN  
 SELECT    
  FACILITYDAILY.FACILITY_ID AS PPFACID, FACILITYDAILY.EU_ID AS PPEUID,    
  FACILITYDAILY.EU_DAILY_DATE AS PPDATE  
  ,D_LASTYR.NETUSAGE AS NETUSAGE_LASTYEAR  
  ,D_LASTMO.NETUSAGE AS NETUSAGE_LASTMONTH  
  ,D_LASTWK.NETUSAGE AS NETUSAGE_LASTWEEK  
  ,D_COMP_DAY.NETUSAGE AS NETUSAGE_COMPARE_DAY  
  ,FDC.NETUSAGE AS COMP_DD_NETUSAGE  
  ,FDC.EU_DAILY_DATE AS COMP_DD_EU_DAILY_DATE  
  ,SUBSTRING(DATENAME(Weekday,FDC.EU_DAILY_DATE),1,3) AS COMP_DD_DAYSTR  
  ,BILLED_VOLUME = (  
   SELECT SUM(INVOICEDVOLUME)   
   FROM INVOICEDVOLUME  
    WHERE EU_ID = FACILITYDAILY.EU_ID   
     AND FACILITY_ID = FACILITYDAILY.FACILITY_ID   
     AND EU_DAILY_DATE = FACILITYDAILY.EU_DAILY_DATE   
     AND INVOICERUN <> 0) +   
   (  
   SELECT SUM(INVOICEDVOLUME)  
   FROM INVOICEDOTHERDEL  
   WHERE EU_ID = FACILITYDAILY.EU_ID   
     AND FACILITY_ID = FACILITYDAILY.FACILITY_ID   
     AND EU_DAILY_DATE = FACILITYDAILY.EU_DAILY_DATE   
     AND OTHERCODE = 'B'  
     AND INVIOCERUN <> 0)   
  
     
  ,PENDING_VOLUME =   
   (  
   SELECT SUM(INVOICEDVOLUME)   
   FROM INVOICEDVOLUME  
    WHERE EU_ID = FACILITYDAILY.EU_ID   
     AND FACILITY_ID = FACILITYDAILY.FACILITY_ID   
     AND EU_DAILY_DATE = FACILITYDAILY.EU_DAILY_DATE   
     AND INVOICERUN = 0) +   
   (  
   SELECT SUM(INVOICEDVOLUME)  
   FROM INVOICEDOTHERDEL  
   WHERE EU_ID = FACILITYDAILY.EU_ID   
     AND FACILITY_ID = FACILITYDAILY.FACILITY_ID   
     AND EU_DAILY_DATE = FACILITYDAILY.EU_DAILY_DATE   
     AND OTHERCODE = 'B'  
     AND INVIOCERUN = 0)  
    
 INTO #PRIORPERIODS  
 FROM FACILITYDAILY FACILITYDAILY   
 JOIN FACILITY F ON F.FACILITY_ID=FACILITYDAILY.FACILITY_ID   
 JOIN ENDUSER E ON E.EU_ID=FACILITYDAILY.EU_ID  
 LEFT OUTER JOIN DAILYHDD DD_TODAY ON (DD_TODAY.ZONEID=F.ZONEID  
   AND DD_TODAY.WEATHERDATE = FACILITYDAILY.EU_DAILY_DATE)  
 LEFT OUTER JOIN FACILITYDAILY FDC ON (  
  FDC.EU_ID = FACILITYDAILY.EU_ID   
  AND FDC.FACILITY_ID = FACILITYDAILY.FACILITY_ID   
  AND FDC.EU_DAILY_DATE =   
   (  
    SELECT MAX(COMP_DD.WEATHERDATE)  
    FROM DAILYHDD COMP_DD  
    WHERE COMP_DD.ZONEID = F.ZONEID   
    AND COMP_DD.WEATHERDATE <= DATEADD(DAY,-2,FACILITYDAILY.EU_DAILY_DATE)  
    --AND ISNULL(COMP_DFACILITYDAILY.HDD,0) + ISNULL(COMP_DFACILITYDAILY.CDD,0) = ISNULL(DD_TODAY.HDD,0) + ISNULL(DD_TODAY.CDD,0)  
    AND (  
     (COMP_DD.HDD <> 0 AND COMP_DD.HDD = DD_TODAY.HDD) OR  
     (COMP_DD.CDD <> 0 AND COMP_DD.CDD = DD_TODAY.CDD)  
     )  
       
    AND DATEPART(WEEKDAY, COMP_DD.WEATHERDATE) = DATEPART(WEEKDAY,FACILITYDAILY.EU_DAILY_DATE)  
    )--ORDER BY COMP_DFACILITYDAILY.WEATHERDATE DESC)  
    )  
 LEFT OUTER JOIN FACILITYDAILY D_LASTYR ON (  
  D_LASTYR.EU_ID = FACILITYDAILY.EU_ID   
  AND D_LASTYR.FACILITY_ID = FACILITYDAILY.FACILITY_ID   
  AND D_LASTYR.EU_DAILY_DATE = DATEADD(YEAR,-1,FACILITYDAILY.EU_DAILY_DATE))  
 LEFT OUTER JOIN FACILITYDAILY D_LASTMO ON (  
  D_LASTMO.EU_ID = FACILITYDAILY.EU_ID   
  AND D_LASTMO.FACILITY_ID = FACILITYDAILY.FACILITY_ID   
  AND D_LASTMO.EU_DAILY_DATE = DATEADD(MONTH,-1,FACILITYDAILY.EU_DAILY_DATE))  
 LEFT OUTER JOIN FACILITYDAILY D_LASTWK ON (  
  D_LASTWK.EU_ID = FACILITYDAILY.EU_ID   
  AND D_LASTWK.FACILITY_ID = FACILITYDAILY.FACILITY_ID   
  AND D_LASTWK.EU_DAILY_DATE = DATEADD(DAY,-7,FACILITYDAILY.EU_DAILY_DATE))  
 LEFT OUTER JOIN FACILITYDAILY D_COMP_DAY ON (  
  D_COMP_DAY.EU_ID = FACILITYDAILY.EU_ID   
  AND D_COMP_DAY.FACILITY_ID = FACILITYDAILY.FACILITY_ID   
  AND D_COMP_DAY.EU_DAILY_DATE = @COMP_DATE)  
   
 WHERE     
  (@FACILITY_ID = 0 OR FACILITYDAILY.FACILITY_ID =  @FACILITY_ID)  
  AND (@LDCID = 0 OR FACILITYDAILY.LDCID = @LDCID)  
  AND (@POOLID = 0 OR FACILITYDAILY.LDC_POOLING_ID = @POOLID)  
  AND FACILITYDAILY.EU_DAILY_DATE BETWEEN @SDATE AND @EDATE   
    
    
 SELECT    
   FACILITYDAILY.FACILITY_ID  
  ,FACILITYDAILY.EU_ID  
  ,FACILITYDAILY.EU_DAILY_DATE  
  ,F.NAME AS FACNAME  
  ,F.ACCOUNTNUMBER AS FACACCOUNT  
  ,F.ZONEID  
  ,E.NAME AS ENDUSERNAME  
  ,E.STATUS AS EUSTATUS  
  ,F.ENDUSEROPTIONALID3ALIAS  
  ,F.ENDUSEROPTIONALID4ALIAS  
    
  ,SUBSTRING(DATENAME(Weekday,FACILITYDAILY.EU_DAILY_DATE),1,3) AS [DayOfWeek]  
  ,FACILITYDAILY.STATUS  
  ,FACILITYDAILY.PROJECTEDUSAGE  
  ,FACILITYDAILY.AI_PROJECTION  
  ,AI.ForecastedUsage AS LATEST_AI_FORECAST  
  ,FACILITYDAILY.OVERRIDEUSAGE  
  ,FACILITYDAILY.NOMINATEDDELIVERY  
  ,FACILITYDAILY.BTU  
  ,FACILITYDAILY.PRESSURE  
  ,FACILITYDAILY.TEMPERATURE  
  ,FACILITYDAILY.OTHERFACTORS  
  ,FACILITYDAILY.AVGPERIODUSAGE  
  ,FACILITYDAILY.METEREDUSAGEPERLDC  
  ,FACILITYDAILY.LDCID  
  ,FACILITYDAILY.LDC_POOLING_ID  
  ,FACILITYDAILY.STOREDORIGNOM  
  ,FACILITYDAILY.FIRSTOFMONTHNOMLOCKED  
  ,FACILITYDAILY.LOCKNOMINATEDDELIVERY  
  ,FACILITYDAILY.NETUSAGE  
    
  ,PP.NETUSAGE_LASTYEAR  
  ,PP.NETUSAGE_LASTMONTH  
  ,PP.NETUSAGE_LASTWEEK  
  ,PP.NETUSAGE_COMPARE_DAY  
  ,PP.COMP_DD_NETUSAGE  
  ,PP.COMP_DD_EU_DAILY_DATE  
  ,PP.COMP_DD_DAYSTR  
  ,PP.BILLED_VOLUME  
  ,PP.PENDING_VOLUME   
    
  ,FACILITYDAILY.NOTES  
  ,DD_TODAY.HIGHTEMP  
  ,DD_TODAY.LOWTEMP  
  ,DD_TODAY.PMWINDSPEED  
  ,DD_TODAY.PMPRECIP  
  ,DD_TODAY.FORECAST  
  ,DD_TODAY.HDD AS TODAYSHDD  
  ,DD_TODAY.CDD AS TODAYSCDD  
    
  ,E.SELFNOMINATING  
  ,H.DESCRIPTION AS HOLIDAYNAME  
  ,PA.CONTRACTMDQ  
  ,PA.ORIGINALBASISVOLUME AS CONTRACTQTY  
  ,DAILYFACMDQ=(  
    SELECT SUM(DAILYVALUE)  
   FROM DAILYFACVALUES V  
   JOIN LDCDAILYTYPE DT ON (DT.LDCID = FACILITYDAILY.LDCID AND DT.DAILYTYPEID=V.DAILYTYPEID)  
   WHERE V.FACILITY_ID=FACILITYDAILY.FACILITY_ID  
   AND V.EU_DAILY_DATE = FACILITYDAILY.EU_DAILY_DATE  
   AND DT.USEASDAILYMAX = 'Y')  
  ,DAILYEUMDQ=(  
    SELECT SUM(DAILYVALUE)  
   FROM DAILYEUVALUES V  
   JOIN LDCDAILYTYPE DT ON (DT.LDCID = FACILITYDAILY.LDCID AND DT.DAILYTYPEID=V.DAILYTYPEID)  
   WHERE V.EU_ID=FACILITYDAILY.EU_ID  
   AND V.EU_DAILY_DATE = FACILITYDAILY.EU_DAILY_DATE  
   AND DT.USEASDAILYMAX = 'Y')  
  ,PCTPROJECTEDOFNET = ROUND(CASE FACILITYDAILY.NETUSAGE   
   WHEN 0 THEN 0  
   ELSE (FACILITYDAILY.NETUSAGE - FACILITYDAILY.PROJECTEDUSAGE) / FACILITYDAILY.NETUSAGE   
   END  * 100,2)  
  ,PCTOVERRIDEOFNET = ROUND(CASE FACILITYDAILY.NETUSAGE   
   WHEN 0 THEN 0  
   ELSE (FACILITYDAILY.NETUSAGE - FACILITYDAILY.OVERRIDEUSAGE) / FACILITYDAILY.NETUSAGE   
   END * 100,2)  
  ,ISNULL(NOTE.PROJECTION_CALCULATION_NOTE,'No Calculation Notes Available') as PROJECTION_CALCULATION_NOTE  
  , AV.AVGHDD   
  ,AV.AVGCDD  
  ,AV.AVGHIGH  
  ,AV.AVGLOW   
 FROM FACILITYDAILY AS FACILITYDAILY   
 JOIN #PRIORPERIODS PP ON (PP.PPEUID = FACILITYDAILY.EU_ID AND PP.PPFACID = FACILITYDAILY.FACILITY_ID AND PP.PPDATE = FACILITYDAILY.EU_DAILY_DATE)  
 JOIN FACILITY F ON F.FACILITY_ID=FACILITYDAILY.FACILITY_ID   
 JOIN ENDUSER E ON E.EU_ID=FACILITYDAILY.EU_ID  
 --2018 08 17 This makes sure we only join 1 holiday set based on the LDC forecasting holidayt set  
 JOIN LDC L ON L.LDCID = F.LDCID  
 JOIN LDCPOOLINGACCOUNT P ON P.LDC_POOLING_ID = E.LDC_POOLING_ID  
 LEFT OUTER JOIN HOLIDAY H ON (H.HOLIDAYDATE = FACILITYDAILY.EU_DAILY_DATE AND EXISTS (SELECT 1 FROM HOLIDAY_SET_HOLIDAY HSH   
   WHERE HSH.HOLIDAY_SET_ID = L.FORECASTING_HOLIDAY_SET_ID AND HSH.HOLIDAY_ID = H.HOLIDAYID))  
 LEFT OUTER JOIN DAILYHDD DD_TODAY ON (DD_TODAY.ZONEID=F.ZONEID  
   AND DD_TODAY.WEATHERDATE = FACILITYDAILY.EU_DAILY_DATE)  
 LEFT OUTER JOIN PRICEAGREEMENT PA ON (  
   PA.EU_ID = FACILITYDAILY.EU_ID    
   AND FACILITYDAILY.EU_DAILY_DATE BETWEEN PA.EFFECTIVEDATE AND PA.ENDDATE  
   AND PA.PRICINGMETHOD = 'T')  
 LEFT OUTER JOIN FACILITYDAILY_PROJECTION_NOTE NOTE ON (  
  NOTE.EU_ID = FACILITYDAILY.EU_ID AND  
  NOTE.EU_DAILY_DATE = FACILITYDAILY.EU_DAILY_DATE AND  
  NOTE.FACILITY_ID = FACILITYDAILY.FACILITY_ID)  
 LEFT OUTER JOIN AVERAGEHDD AV ON (AV.ZONEID = F.ZONEID AND AV.DAYOFYEAR = dbo.udf_DateToDOY(FACILITYDAILY.EU_DAILY_DATE))  
 LEFT OUTER JOIN AI_SCORED_RESULTS AI ON (AI.FacilityId = FACILITYDAILY.FACILITY_ID AND AI.Date = FACILITYDAILY.EU_DAILY_DATE AND AI.IS_LATEST = 'Y')
 LEFT OUTER JOIN MARKETER M ON M.MARKETER_ID = P.MARKETER_ID
 WHERE     
  (@FACILITY_ID = 0 OR FACILITYDAILY.FACILITY_ID =  @FACILITY_ID)  
  AND (@LDCID = 0 OR FACILITYDAILY.LDCID = @LDCID)  
  AND (@POOLID = 0 OR FACILITYDAILY.LDC_POOLING_ID = @POOLID) 
  AND (@MARKETER_ID = 0 OR M.MARKETER_ID = @MARKETER_ID) 
  AND FACILITYDAILY.EU_DAILY_DATE BETWEEN @SDATE AND @EDATE   
 ORDER BY F.NAME, FACILITYDAILY.EU_DAILY_DATE  
 --ORDER BY   
 --CASE @SORT_ORDER   
 -- WHEN 0 THEN F.NAME  
 --   WHEN 1 THEN F.ACCOUNTNUMBER   
 --   WHEN 2 THEN F.ENDUSEROPTIONALID3ALIAS  
 --   WHEN 3 THEN F.ENDUSEROPTIONALID4ALIAS  
 --   END, FACILITYDAILY.EU_DAILY_DATE  
END
GO

grant exec on LSP_SEL_DAILY_METER_FACILITY to EnTrackUser
GO


exec dbo.LSP_UTIL_LOG_SQL_SCRIPT
	@ScriptFileName = '2020 05 16 LSP_SEL_DAILY_METER_FACILITY.sql',
	@Author = 'Mark Lloret',
	@Description = 'Description.'
