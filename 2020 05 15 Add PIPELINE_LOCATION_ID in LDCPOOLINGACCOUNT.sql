if not exists (SELECT * FROM SYSCOLUMNS 
  WHERE NAME = 'PIPELINE_LOCATION_ID' AND ID = (
  SELECT ID FROM SYSOBJECTS WHERE NAME = 'LDCPOOLINGACCOUNT' AND XTYPE = 'U'))
ALTER TABLE LDCPOOLINGACCOUNT ADD
	PIPELINE_LOCATION_ID INT NULL,
	CONSTRAINT FK_PIPELINE_LOCATION_ID FOREIGN KEY (PIPELINE_LOCATION_ID)
		REFERENCES PIPELINE_LOCATION(PIPELINE_LOCATION_ID)
GO
     
exec dbo.LSP_UTIL_LOG_SQL_SCRIPT
	@ScriptFileName = '2020 05 15 Add PIPELINE_LOCATION_ID in LDCPOOLINGACCOUNT.sql',
	@Author = 'Sachin Borgaonkar',
	@Description = 'Description.'