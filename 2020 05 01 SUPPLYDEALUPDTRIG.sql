if exists (select * from dbo.sysobjects 
	where id = object_id(N'[dbo].SUPPLYDEALUPDTRIG') 
		and OBJECTPROPERTY(id, N'IsTrigger') = 1)
  drop TRIGGER [dbo].SUPPLYDEALUPDTRIG
GO

CREATE TRIGGER [dbo].[SUPPLYDEALUPDTRIG] ON [dbo].[SUPPLYDEAL]
FOR UPDATE
AS
    DECLARE @RowCount INT;
    DECLARE @NEW_SUPPLIER_ID INT
    DECLARE @NEW_SUPPLY_DEAL_ID INT
    DECLARE @NEW_COSTPOOLMETHOD INT
    DECLARE @NEW_ENDDATE DATEFIELD
    DECLARE @NEW_COUNTERPARTYTYPE SHORTNAME
    DECLARE @NEW_STATUS INT
    DECLARE
			@ACCOUNTINGGROUPID INT, 
			@BUSINESS_UNIT_ID INT, 
			@SUPPLYREGIONID INT,
			@PORTFOLIO_ID INT,
			@PERIOD_TYPE INT,
			@STRATEGY_ID INT,
			@PIPELINE_ID INT,
			@PIPELINE_LOCATION_ID INT,
			@TRXTYPE VARCHAR(25),
			@INSTRUMENT_ID INT
    DECLARE @OLD_SUPPLIER_ID INT
    DECLARE @OLD_SUPPLY_DEAL_ID INT
    DECLARE @OLD_ENDDATE DATEFIELD
    DECLARE @OLD_COUNTERPARTYTYPE SHORTNAME
    DECLARE @OLD_STATUS INT
	DECLARE 
	        @NEW_VOL DAILYVOLUMES,
			@OLD_VOL DAILYVOLUMES
			
    DECLARE NEW CURSOR LOCAL
        FOR SELECT SUPPLIER_ID, SUPPLY_DEAL_ID, ENDDATE, DEALSTATUS, ACCOUNTINGGROUPID, 
					BUSINESS_UNIT_ID, SUPPLYREGIONID, PORTFOLIO_ID, STRATEGY_ID,
					PIPELINE_ID, PIPELINE_LOCATION_ID, TRXTYPE, DAILYVOLUME, INSTRUMENT_ID, PERIOD_TYPE_REF
       FROM INSERTED
    DECLARE OLD CURSOR LOCAL
        FOR SELECT SUPPLIER_ID, SUPPLY_DEAL_ID, ENDDATE, DEALSTATUS, DAILYVOLUME
       FROM DELETED
BEGIN
    OPEN NEW
    FETCH NEW INTO @NEW_SUPPLIER_ID, @NEW_SUPPLY_DEAL_ID, @NEW_ENDDATE, @NEW_STATUS,
					@ACCOUNTINGGROUPID, @BUSINESS_UNIT_ID, @SUPPLYREGIONID, @PORTFOLIO_ID, @STRATEGY_ID, 
					@PIPELINE_ID, @PIPELINE_LOCATION_ID, @TRXTYPE, @NEW_VOL, @INSTRUMENT_ID, @PERIOD_TYPE
    OPEN OLD
    FETCH OLD INTO @OLD_SUPPLIER_ID, @OLD_SUPPLY_DEAL_ID, @OLD_ENDDATE, @OLD_STATUS, @OLD_VOL
    WHILE (@@FETCH_STATUS = 0)
    BEGIN
      DECLARE @DATE_IS_CLOSED CHAR(1)
       EXEC SP_TEST_CLOSEDATE @NEW_ENDDATE,
          @out_PRIORTOCLOSE = @DATE_IS_CLOSED OUTPUT
        
       IF @DATE_IS_CLOSED = 'Y'
       BEGIN
         RAISERROR('You may not modify records prior to Accounting Close Date',16,1)
         ROLLBACK RETURN 
       END
       EXEC SP_TEST_CLOSEDATE @OLD_ENDDATE,
          @out_PRIORTOCLOSE = @DATE_IS_CLOSED OUTPUT
        
       IF @DATE_IS_CLOSED = 'Y'
       BEGIN
         RAISERROR('You may not modify records prior to Accounting Close Date',16,1)
         ROLLBACK RETURN 
       END
      /*
       * Parent Update@ RESTRICT
       */
      IF (@OLD_Supply_Deal_ID <> @NEW_Supply_Deal_ID)
      BEGIN
          SELECT @RowCount = COUNT(*)
            FROM SupplyLeg ch
           WHERE ch.Supply_Deal_ID = @OLD_Supply_Deal_ID

          IF (@RowCount > 0)
          BEGIN
            RAISERROR('Update Supply Deal Failed: Supplier Leg dependency.', 16, 1) /* 'Exception E_UPD_SupplyLeg */
            ROLLBACK RETURN
          END
      END
      /*
       * Child Update@ RESTRICT
       */
      IF (@OLD_Supplier_ID <> @NEW_Supplier_ID)
      BEGIN
          IF @NEW_COUNTERPARTYTYPE = 'Supplier'
            SELECT @RowCount = COUNT(*)
              FROM Supplier pr
             WHERE pr.Supplier_ID = @NEW_Supplier_ID

          IF @NEW_COUNTERPARTYTYPE = 'End User'
            SELECT @RowCount = COUNT(*)
              FROM ENDUSER pr
             WHERE pr.EU_ID = @NEW_Supplier_ID
          IF (@RowCount < 1)
          BEGIN
            IF @NEW_COUNTERPARTYTYPE = 'Supplier'
              RAISERROR('Update Supply Deal Failed: Supplier does not exist.', 16, 1) /* 'Exception E_UPD_Supplier */
            IF @NEW_COUNTERPARTYTYPE = 'End User'  
              RAISERROR('Update Supply Deal Failed: End User does not exist', 16,1)
            ROLLBACK RETURN
          END
      END
      
      IF (@OLD_STATUS <> @NEW_STATUS) AND (@NEW_STATUS = 5)
      BEGIN
				SELECT @RowCount = COUNT(*) 
					FROM SUPPLYTIER ST
					JOIN MARKETTIER MT ON MT.SUPPLYTIERID = ST.SUPPLYTIERID
					WHERE ST.SUPPLY_DEAL_ID = @NEW_SUPPLY_DEAL_ID
				IF (@RowCount > 0)
          BEGIN
            RAISERROR('Market tiers are linked to this deal.  Supply Deal may not be cancelled.  ', 16,1)
            ROLLBACK RETURN
          END
      END
      /*If we are not allowing tier level assignments to wacog pools, enforce this here*/
      IF @NEW_COSTPOOLMETHOD <> 2
        UPDATE SUPPLYTIER
           SET WACOGPOOLID = NULL
         WHERE SUPPLY_DEAL_ID = @NEW_SUPPLY_DEAL_ID

			if dbo.[udf_CheckSysRequired_AccountingGroup] () = 'Y' 
      BEGIN
				SELECT @Rowcount = count(*)
				from ACCOUNTINGGROUP
				WHERE ACCOUNTINGGROUPID = @ACCOUNTINGGROUPID
				IF (@RowCount < 1)
				BEGIN
					RAISERROR('System requires a valid accounting group for each supply deal.', 16, 1) /* 'Exception E_INS_Supplier */
					ROLLBACK RETURN
				END
      END
      if dbo.[udf_CheckSysRequired_BusinessUnit] () = 'Y' 
      BEGIN
				SELECT @Rowcount = count(*)
				from BUSINESS_UNIT
				WHERE BUSINESS_UNIT_ID = @BUSINESS_UNIT_ID
				IF (@RowCount < 1)
				BEGIN
					RAISERROR('System requires a valid business unit for each supply deal.', 16, 1) /* 'Exception E_INS_Supplier */
					ROLLBACK RETURN
				END
      END
      if dbo.[udf_CheckSysRequired_SupplyRegion] () = 'Y' 
      BEGIN
				SELECT @Rowcount = count(*)
				from SUPPLYREGION
				WHERE SUPPLYREGIONID = @SUPPLYREGIONID
				IF (@RowCount < 1)
				BEGIN
					RAISERROR('System requires a valid supply reqion for each supply deal.', 16, 1) /* 'Exception E_INS_Supplier */
					ROLLBACK RETURN
				END
      END
      
      if dbo.[udf_CheckSysRequired_Portfolio] () = 'Y' 
			BEGIN
				SELECT @Rowcount = count(*)
				from PORTFOLIO
				WHERE PORTFOLIO_ID = @PORTFOLIO_ID
				IF (@RowCount < 1)
				BEGIN
					RAISERROR('System requires a portfolio for each Supply Deeal.', 16, 1) 
					ROLLBACK RETURN
				END
			END
			
			if dbo.[udf_CheckSysRequired_Strategy] () = 'Y' 
			BEGIN
				SELECT @Rowcount = count(*)
				from STRATEGY
				WHERE STRATEGY_ID = @STRATEGY_ID
				IF (@RowCount < 1)
				BEGIN
					RAISERROR('System requires a strategy for each Supply Deeal.', 16, 1) 
					ROLLBACK RETURN
				END
			END

			--10-08-2018 new conditional enforcement of curve
			if dbo.[udf_CheckSysRequired_MarkToMarket] () = 'Y' 
			BEGIN
				SELECT @Rowcount = count(*)
				from PIPELINE  
				WHERE PIPELINE_ID = @PIPELINE_ID
				IF (@RowCount < 1)
				BEGIN
					RAISERROR('System market curve setting requires Pipeline for supply deals.', 16, 1) 
					ROLLBACK RETURN
				END

				SELECT @Rowcount = count(*)
				from PIPELINE_LOCATION    
				WHERE PIPELINE_LOCATION_ID = @PIPELINE_LOCATION_ID
				IF (@RowCount < 1)
				BEGIN
					RAISERROR('System market curve setting requires pipeline location.', 16, 1) 
					ROLLBACK RETURN
				END
			END

			IF @INSTRUMENT_ID = 1 AND @OLD_VOL <> @NEW_VOL --Physical 
				EXEC LSP_SUPPLY_INSERT_DAILY_DEAL_VOLS
				     @NEW_SUPPLY_DEAL_ID, 'N', @PERIOD_TYPE

      FETCH NEW INTO @NEW_SUPPLIER_ID, @NEW_SUPPLY_DEAL_ID, @NEW_ENDDATE, @NEW_STATUS,
					@ACCOUNTINGGROUPID, @BUSINESS_UNIT_ID, @SUPPLYREGIONID, @PORTFOLIO_ID, @STRATEGY_ID, 
					@PIPELINE_ID, @PIPELINE_LOCATION_ID, @TRXTYPE, @NEW_VOL, @INSTRUMENT_ID, @PERIOD_TYPE
      FETCH OLD INTO @OLD_SUPPLIER_ID, @OLD_SUPPLY_DEAL_ID, @OLD_ENDDATE, @OLD_STATUS, @OLD_VOL 
    END
    CLOSE NEW
    DEALLOCATE NEW
    CLOSE OLD
    DEALLOCATE OLD
END
GO

ALTER TABLE [dbo].[SUPPLYDEAL] ENABLE TRIGGER [SUPPLYDEALUPDTRIG]
GO

exec dbo.LSP_UTIL_LOG_SQL_SCRIPT
	@ScriptFileName = '2020 05 01 SUPPLYDEALUPDTRIG.sql',
	@Author = 'Kishore Sharma',
	@Description = 'Support for Monthly in SUPPLY_DEAL_DAILY_VOLUME.'

